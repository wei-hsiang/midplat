package com.example.midplat.controller;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.midplat.logic.CustomerLogic;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@Tag(name = "health", description = "the health API")
public class HealthController {

    @Autowired
    private CustomerLogic customerLogic;

    @GetMapping(value = "/ping")
    @Operation(summary = "verify that the service is available")
    @ApiResponses(value = @ApiResponse(responseCode = "200", description = "return PONG means the service is available"))
    public String ping() {
        return "PONG";
    }
}

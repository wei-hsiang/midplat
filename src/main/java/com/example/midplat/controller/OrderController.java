package com.example.midplat.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.midplat.model.dto.PerpetualApplicationStatusDTO;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@Tag(name = "orders", description = "the orders API")
public class OrderController {
	
	private static Logger log = LogManager.getLogger(OrderController.class);
	
	@GetMapping(value = {"/applications/{apyNo}"}, produces = MediaType.APPLICATION_JSON_VALUE)
	@Operation(summary = "")
	@ApiResponses(value = @ApiResponse(responseCode = "200", description = "The service is available"))
	public PerpetualApplicationStatusDTO listApplStatus(	@PathVariable(value = "apyNo") String apyNo,
															@RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") String crtDateBeg,
															@RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") String crtDateEnd) {
		log.error("receive request apyNo={} crtDateBeg={} crtDateEnd={}", apyNo, crtDateBeg, crtDateEnd);
		//TODO
		return new PerpetualApplicationStatusDTO();
	}
	
	@GetMapping(value = {"/applications/sales-allowance/{apyNo}"}, produces = MediaType.APPLICATION_JSON_VALUE)
	@Operation(summary = "")
	@ApiResponses(value = @ApiResponse(responseCode = "200", description = "The service is available"))
	public PerpetualApplicationStatusDTO getSRDSInfo(@PathVariable(value = "apyNo") String apyNo) {
		log.error("receive request apyNo={}", apyNo);
		//TODO
		return new PerpetualApplicationStatusDTO();
	}
	
}

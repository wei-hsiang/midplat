package com.example.midplat.controller;


import java.sql.SQLException;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.midplat.logic.ProductLogic;
import com.example.midplat.model.dto.ProductInfoDTO;
import com.example.midplat.model.dto.ProductPriceDTO;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "product", description = "the product API")
@RestController
public class ProductController {
	
	private static Logger log = LogManager.getLogger(ProductController.class);
	
	@Autowired
	private ProductLogic productLogic;
	
	@GetMapping(value = {"/products/{productID}/info"}, produces = MediaType.APPLICATION_JSON_VALUE)
	@Operation(summary = "")
	@ApiResponses(value = @ApiResponse(responseCode = "200", description = "The service is available", content = @Content(schema = @Schema(implementation = ProductInfoDTO.class))))
	public ProductInfoDTO listProductInfo(@PathVariable(value = "productID") String productID) {
		log.error("receive request productID={}", productID);
		ProductInfoDTO responseDTO = null;
		try {
			responseDTO = productLogic.getProductInfo(productID);
		} catch (SQLException e) {
			log.error(ExceptionUtils.getStackTrace(e));
		}		
		return responseDTO;
	}
	
	@GetMapping(value = {"/products/{productID}/price"}, produces = MediaType.APPLICATION_JSON_VALUE)
	@Operation(summary = "")
	@ApiResponses(value = @ApiResponse(responseCode = "200", description = "The service is available"))
	public ProductPriceDTO getProductPrice( @PathVariable(value = "productID") String productID,
											@RequestParam(required = false, defaultValue = "S") String company) {
		log.error("receive request productID={} company={}", productID, company);
		ProductPriceDTO responseDTO = null;
		try {
			responseDTO = productLogic.getProductPrice(company, productID);
		} catch (SQLException e) {
			log.error(ExceptionUtils.getStackTrace(e));
		}		
		return responseDTO;
	}
	
	
}

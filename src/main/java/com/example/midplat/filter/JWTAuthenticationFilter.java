package com.example.midplat.filter;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.time.LocalDateTime;

import javax.security.auth.message.AuthException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.example.midplat.model.dto.ApiErrorResponse;
import com.example.midplat.util.JwtTokenUtil;

import io.jsonwebtoken.Claims;

@Component
public class JWTAuthenticationFilter implements Filter {
	
	private static Logger log = LogManager.getLogger(JWTAuthenticationFilter.class);
	
	@Value ("${certificate.path}")
	private String certPath;
	
	@Value ("${jwt.signature.publickey}")
	private String cspPK;
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest httpRequest = (HttpServletRequest) request;
		String url = httpRequest.getRequestURL().toString();
		if (url.contains("/swagger-ui") || url.contains("/api-docs")) {
			/**
			 * urlhttp://localhost:8080/v1/swagger-ui/*
			 * http://localhost:8080/v1/api-docs/*
			 * 	應排除 Paths /swagger-ui/* 及 /api-docs/*  驗證request header
			 */
			chain.doFilter(request, response);
		} else {

			//驗證兩個header (Authorization、X-PLAT)
			String header_authorization = httpRequest.getHeader("Authorization");
			String jwt = header_authorization.split(" ")[1];
			String platform = httpRequest.getHeader("X-PLAT").toLowerCase();
			log.debug("header_authorization{} platform{}", header_authorization, platform);
			try {
				if (StringUtils.isBlank(header_authorization) || StringUtils.isBlank(platform)) {
					throw new AuthException("request header is null or empty");
				}
				
				//TODO 對應哪一個平台使用的public key, 暫時先使用@Value直接拿
				PublicKey pk = null;
				if ("AAA".contains(platform)) {
					pk = JwtTokenUtil.loadPublicKey(certPath, cspPK, "RSA");
				} else if ("BBB".contains(platform)) {
					
				}
				
				//驗證JWT
				Claims c = JwtTokenUtil.validateToken(pk ,jwt);
				
				chain.doFilter(request, response);
			} catch (AuthException e) {
				ApiErrorResponse res = new ApiErrorResponse.ApiErrorResponseBuilder()
										                .withStatus(HttpStatus.NON_AUTHORITATIVE_INFORMATION)
										                .withError_code("BAD_DATA")
										                .withMessage(e.getLocalizedMessage())
										                .withDetail(e + e.getMessage())
										                .atTime(LocalDateTime.now())
										                .build();
				String resp = new Gson().toJson(res);
				PrintWriter printWriter = response.getWriter();
				response.setContentType("application/json;charset=UTF-8");
				printWriter.write(resp);
				printWriter.flush();
			} catch (NoSuchAlgorithmException e) {
				log.error(ExceptionUtils.getStackTrace(e));
			} catch (InvalidKeySpecException e) {
				log.error(ExceptionUtils.getStackTrace(e));
			}
		}
	}
	
}

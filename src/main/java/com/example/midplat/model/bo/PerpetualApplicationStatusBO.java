package com.example.midplat.model.bo;

import com.example.midplat.enums.OrderStatusEnums;

public class PerpetualApplicationStatusBO {

	private String apyNo;
	private OrderStatusEnums status;

	public PerpetualApplicationStatusBO() {
		super();
	}

	public PerpetualApplicationStatusBO(String apyNo, OrderStatusEnums status) {
		super();
		this.apyNo = apyNo;
		this.status = status;
	}

	public String getApyNo() {
		return apyNo;
	}

	public void setApyNo(String apyNo) {
		this.apyNo = apyNo;
	}

	public OrderStatusEnums getStatus() {
		return status;
	}

	public void setStatus(OrderStatusEnums status) {
		this.status = status;
	}

}

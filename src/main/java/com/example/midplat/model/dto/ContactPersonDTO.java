package com.example.midplat.model.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.example.midplat.model.bo.ContactPersonBO;

import io.swagger.v3.oas.annotations.media.Schema;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ContactPersonDTO {
	private int totalCount; // 總筆數
	private int pages; // 總共幾頁
	private int pageSize; // 一頁有幾筆
	private int currentPage; // 當前頁數
	private List<ContactPersonBO> items;

	public ContactPersonDTO() {
		super();
	}

	public ContactPersonDTO(int totalCount, int pages, int pageSize, int currentPage, List<ContactPersonBO> items) {
		super();
		this.totalCount = totalCount;
		this.pages = pages;
		this.pageSize = pageSize;
		this.currentPage = currentPage;
		this.items = items;
	}
	
	@Schema(example = "", description = "總筆數")
	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	@Schema(example = "", description = "總頁數")
	public int getPages() {
		return pages;
	}

	public void setPages(int pages) {
		this.pages = pages;
	}
	
	@Schema(example = "", description = "單頁最多筆數")
	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	
	@Schema(example = "", description = "當前頁數")
	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}
	
	@Schema(example = "", description = "資料")
	public List<ContactPersonBO> getItems() {
		return items;
	}

	public void setItems(List<ContactPersonBO> items) {
		this.items = items;
	}

}

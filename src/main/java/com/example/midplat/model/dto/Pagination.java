package com.example.midplat.model.dto;

public class Pagination {
	private int totalCount; //總筆數
	private int pages; //總共幾頁
	private int pageSize; //一頁有幾筆
	private int currentPage; //當前頁數
	private Object[] items; //資料
	
	public Pagination() {
		super();
	}
	
	public Pagination(int totalCount, int pages, int pageSize, int currentPage, Object[] items) {
		super();
		this.totalCount = totalCount;
		this.pages = pages;
		this.pageSize = pageSize;
		this.currentPage = currentPage;
		this.items = items;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public int getPages() {
		return pages;
	}

	public void setPages(int pages) {
		this.pages = pages;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public Object[] getItems() {
		return items;
	}

	public void setItems(Object[] items) {
		this.items = items;
	}

}

package com.example.midplat.model.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.example.midplat.model.bo.ProductInfoBO;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductInfoDTO {
	
	private List<ProductInfoBO> productList;

	public ProductInfoDTO() {
		super();
	}

	public ProductInfoDTO(List<ProductInfoBO> productList) {
		super();
		this.productList = productList;
	}

	public List<ProductInfoBO> getProductList() {
		return productList;
	}

	public void setProductList(List<ProductInfoBO> productList) {
		this.productList = productList;
	}

}

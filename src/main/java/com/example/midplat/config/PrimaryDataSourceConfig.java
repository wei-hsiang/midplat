package com.example.midplat.config;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
@MapperScan(basePackages = "com.example.midplat.dao.pri.mapper", sqlSessionFactoryRef = "primarySessionFactory")
public class PrimaryDataSourceConfig {

	private static Logger log = LogManager.getLogger(PrimaryDataSourceConfig.class);
	
	//From the configuration file, get the relevant configuration of the database
	@Bean(name = "primaryDataSourceProperties")
	@Primary
	@ConfigurationProperties(prefix = "spring.datasource.primary")
	public DataSourceProperties primaryDataSourceProperties() {
		return new DataSourceProperties();
	}

	@Bean(name = "primaryDataSource")
	@Primary
	public DataSource primaryDataSource(@Qualifier("primaryDataSourceProperties") DataSourceProperties primaryDataSourceProperties) {
		final HikariConfig hikariConfig = new HikariConfig();
		hikariConfig.setDriverClassName(primaryDataSourceProperties.getDriverClassName());
		hikariConfig.setJdbcUrl(primaryDataSourceProperties.getUrl());
		hikariConfig.setUsername(primaryDataSourceProperties.getUsername());
		hikariConfig.setPassword(primaryDataSourceProperties.getPassword());
		hikariConfig.setMinimumIdle(3);
		hikariConfig.setMaximumPoolSize(10);
		hikariConfig.setConnectionTimeout(30000);
		hikariConfig.setConnectionTestQuery("SELECT 1 FROM dual");
		hikariConfig.setIdleTimeout(60000);
		return new HikariDataSource(hikariConfig);
	}

	@Bean(name = "primarySessionFactory")
	@Primary
	public SqlSessionFactory primarySessionFactory(@Qualifier("primaryDataSource") DataSource dataSource)
			throws Exception {
		SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
		bean.setDataSource(dataSource);
		bean.setMapperLocations(
				new PathMatchingResourcePatternResolver().getResources("classpath*:mapper/primary/*.xml"));
		return bean.getObject();
	}

	@Bean(name = "primaryTransactionManager")
	@Primary
	public DataSourceTransactionManager primaryTransactionManager(
			@Qualifier("primaryDataSource") DataSource dataSource) {
		return new DataSourceTransactionManager(dataSource);
	}

	@Bean(name = "primarySessionTemplate")
	@Primary
	public SqlSessionTemplate primarySessionTemplate(
			@Qualifier("primarySessionFactory") SqlSessionFactory sqlSessionFactory) throws Exception {
		return new SqlSessionTemplate(sqlSessionFactory);
	}

}

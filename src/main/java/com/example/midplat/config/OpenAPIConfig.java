package com.example.midplat.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;

/**
 * 	官方教學網站 https://springdoc.org/
 *  API doc	http://server:port/context-path/swagger-ui.html 
 */
@Configuration
public class OpenAPIConfig {

	@Bean
	public OpenAPI springDocAPI(@Value("${springdoc.version}") String appVersion) {
		return new OpenAPI()
//				.components(new Components().addSecuritySchemes("basicScheme",
//						new SecurityScheme().type(SecurityScheme.Type.HTTP).scheme("basic")))
				.info(new Info()
						.title("middle platform API")
						.description("data exchange")
						.version(appVersion)
						.license(new License().name("Apache 2.0").url("http://springdoc.org")));
	}
	
}

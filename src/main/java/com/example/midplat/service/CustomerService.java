package com.example.midplat.service;

import java.sql.SQLException;


public interface CustomerService {
	public int insert() throws SQLException;
	public int update() throws SQLException;
	public int delete( ) throws SQLException;
	public String getCustomerProfile(String billTo) throws SQLException;
}

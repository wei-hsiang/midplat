package com.example.midplat.service.impl;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.example.midplat.dao.sec.mapper.ErrcodedMapper;
import com.example.midplat.service.ContactPersonService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;

@Service
public class ContactPersonServiceImpl implements ContactPersonService {
	
	private static Logger log = LogManager.getLogger(ContactPersonServiceImpl.class);

	@Autowired
	private ErrcodedMapper errcodedMapper;

	@Override
	public List<Map<String, Object>> listContactInfo(String divIss, String billTo, String subDiv, int pageNum, int pageSize) throws SQLException {
//		PageHelper.startPage(pageNum, pageSize, false); 關閉查詢總筆數
		PageHelper.startPage(pageNum, pageSize);
		return null;
	}
	
}

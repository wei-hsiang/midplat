package com.example.midplat.service.impl;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.example.midplat.dao.sec.mapper.ErrcodedMapper;
import com.example.midplat.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ErrcodedMapper errcodedMapper;

	@Override
	public void listItemNo(String itemNo) throws SQLException {
		errcodedMapper.selectByPrimaryKey();
	}

	@Override
	public Map<String, Object> getACidByItemNo(String itemNoPrefix, String itemNo) throws SQLException {
		return null;
	}

	@Override
	public List<Map<String, Object>> listSpecWithSeqBySoftWare(String cateID3, String itemNo) throws SQLException {
		return null;
	}
	
	@Override
	public void getProductPrice(String co, String itemNo) throws SQLException {
	}
	
}

package com.example.midplat.service.impl;

import java.sql.SQLException;

import com.example.midplat.dao.sec.mapper.ErrcodedMapper;
import com.example.midplat.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private ErrcodedMapper errcodedMapper;

	@Override
	public int insert( ) throws SQLException {
		return 0;
	}
	
	@Override
	public int update() throws SQLException {
		return 0;
	}
	
	@Override
	public int delete( ) throws SQLException {
		return 0;
	}
	
	@Override
	public String getCustomerProfile(String billTo) throws SQLException {
		return "";
	}
	
}

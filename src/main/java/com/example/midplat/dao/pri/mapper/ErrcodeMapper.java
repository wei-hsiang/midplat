package com.example.midplat.dao.pri.mapper;

import java.util.List;

import com.example.midplat.dao.pri.entity.ErrcodeEntity;


public interface ErrcodeMapper {

//	public String getCurrentDate();
	public List<ErrcodeEntity> findAllByCodeType();
	public int insert(ErrcodeEntity entity);
	public int update(ErrcodeEntity entity);
	public int delete(ErrcodeEntity entity);
}
